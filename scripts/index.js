"use strict";

window.onload = function () {
    const ptrcalcbtn = document.getElementById("calculaterate");
    ptrcalcbtn.onclick = calctotalchrg;
}

function calctotalchrg() {
    const ptrnbrofdays = document.getElementById("numberofdays");
    const ptrett = document.getElementById("electronictolltag");
    const ptrgpst = document.getElementById("gps");
    const ptrrsa = document.getElementById("roadsideassistance");
    const ptrunder25 = document.getElementById("under25");
    const ptrover25 = document.getElementById("over25") ;
    const ptrcarrentalrate = document.getElementById("carrentalrate") ;
    const ptroptionsrate = document.getElementById("optionsrate");
    const ptrunder25charge = document.getElementById("under25charge") ;
    const ptrtotaldue = document.getElementById("totaldue") ;

    let baserate = 29.99;
    let tollrate = 0;
    let gpsrate = 0;
    let roadsiderate = 0;
    let under25rate = baserate * .3;
    let nmbrofdays = Number(ptrnbrofdays.value);
    alert(nmbrofdays) ;
    if (ptrett.checked) {
        tollrate = 3.95;
    }
    
    if (ptrgpst.checked) {
        gpsrate = 2.95;
    }
    
    if (ptrrsa.checked) {
        roadsiderate = 2.95;
    }
    
    if (ptrunder25.checked || (!ptrunder25.checked && !ptrover25.checked)) {
        under25rate = 0;
    }

    let rentalchrg = baserate * nmbrofdays;
    let optionchrg = (tollrate + gpsrate +  roadsiderate) * nmbrofdays;
    let under25chrg = under25rate *  nmbrofdays;
    let totalchrg = (baserate + tollrate + gpsrate + roadsiderate + under25rate) * nmbrofdays;

    ptrcarrentalrate.innerHTML = rentalchrg ;
    ptroptionsrate.innerHTML = optionchrg ;
    ptrunder25charge.innerHTML =  under25chrg;
    ptrtotaldue.innerHTML = totalchrg;
    return false ;
}